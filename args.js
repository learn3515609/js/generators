function* sumGenerator() {
  const a = yield 'a';
  const b = yield 'b';
  yield a + b;
}

const gen = sumGenerator();
console.log(gen.next());
console.log(gen.next(3));
console.log(gen.next(4));