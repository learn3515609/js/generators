function* getCatets() {
  const a = yield 'input catet 1'
  const b = yield 'input catet 2'
  return [a, b];
}

function* pifagorGenerator(){
  const catets = yield* getCatets();
  yield Math.sqrt(catets[0]**2 + catets[1]**2)
}

const gen = pifagorGenerator()

console.log(gen.next())
console.log(gen.next(3))
console.log(gen.next(4))