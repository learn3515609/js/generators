async function* queueGenerator(n) {
  for(let i = 0; i < n; i++){
    await new Promise(resolve => setTimeout(resolve, 500));
    yield i;
    if(i === n - 1) i = 0;
  }
}

(async () => {
  const gen = queueGenerator(5);
  for await(let k of gen) {
    console.log(k)
  }
})();